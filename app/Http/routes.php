<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Stevebauman\Inventory\Models\Inventory;

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::get('inventory/addMetric', 'InventoryController@addMetric');
Route::get('inventory/addCategory', 'InventoryController@addCategory');
Route::get('inventory/addInventory', 'InventoryController@addInventory');
Route::get('inventory/getMetrics', 'InventoryController@getMetrics');

Route::get('inventory', function() {
    //$items = Inventory::find(1);
    //$items = Inventory::all();
    //$items = Inventory::orderBy( 'name', 'DESC' )->get();
    $category_id = 1;
    
    $items = Inventory::where( 'category_id', '=', $category_id )->orderBy( 'name', 'ASC' )->take(2)->skip(2)->get();
    
    $noItems = $items->count();
    
    echo $noItems.' items in list<br />';
    
    foreach ( $items as $item ) {
        $category = $item->category;
        echo '<pre>';        
        echo "Item ${item['id']} ${item['name']} ${item['description']}<br />";
        echo "Category ${category['id']} ${category['name']}<br /><br />";
        echo '</pre>';       
       
    }
    
    DB::transaction( function() {
        $x = new Model;
        $x->a = '1';
        $x->b = 'afasdf';
        $x->save();    
    } );
    
    DB::beginTransaction();
    try {
        $x = new Model;
        $x->a = '1';
        $x->b = 'afasdf';
        $x->save();  
        DB::commit();
    } catch ( exception as $e ) {
        DB::rollback();
    }
});